def uppercaseversion(letter):
    upperletters = [ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
     'U', 'V', 'W', 'X', 'Y', 'Z']
    if letter in upperletters:
        return letter
    elif letter == 'a':
        return 'A'
    elif letter =='b':
        return 'B'
    elif letter =='c':
        return 'C'
    elif letter =='d':
        return 'D'
    elif letter =='e':
        return 'E'
    elif letter =='f':
        return 'F'
    elif letter =='g':
        return 'G'
    elif letter =='h':
        return 'H'
    elif letter =='i':
        return 'I'
    elif letter =='j':
        return 'J'
    elif letter =='k':
        return 'K'
    elif letter =='l':
        return 'L'
    elif letter =='m':
        return 'M'
    elif letter =='n':
        return 'N'
    elif letter =='o':
        return 'O'
    elif letter =='p':
        return 'P'
    elif letter =='q':
        return 'Q'
    elif letter =='r':
        return 'R'
    elif letter =='s':
        return 'S'
    elif letter =='t':
        return 'T'
    elif letter =='u':
        return 'U'
    elif letter =='v':
        return 'V'
    elif letter =='w':
        return 'W'
    elif letter =='x':
        return 'X'
    elif letter =='y':
        return 'Y'
    elif letter =='z':
        return 'Z'
    else:
        return "Not Valid"






def lowercase(letter):
    lowerletters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                      't', 'u', 'v', 'w','x', 'y', 'z']
    if letter in lowerletters:
        return letter
    elif letter == 'A':
        return 'a'
    elif letter == 'B':
        return 'b'
    elif letter == 'C':
        return 'c'
    elif letter == 'D':
        return 'd'
    elif letter == 'E':
        return 'e'
    elif letter == 'F':
        return 'f'
    elif letter == 'G':
        return 'g'
    elif letter == 'H':
        return 'h'
    elif letter == 'I':
        return 'i'
    elif letter == 'J':
        return 'j'
    elif letter == 'K':
        return 'k'
    elif letter == 'L':
        return 'l'
    elif letter == 'M':
        return 'm'
    elif letter == 'N':
        return 'n'
    elif letter == 'O':
        return 'o'
    elif letter == 'P':
        return 'p'
    elif letter == 'Q':
        return 'q'
    elif letter == 'R':
        return 'r'
    elif letter == 'S':
        return 's'
    elif letter == 'T':
        return 't'
    elif letter == 'U':
        return 'u'
    elif letter == 'V':
        return 'v'
    elif letter == 'W':
        return 'w'
    elif letter == 'X':
        return 'x'
    elif letter == 'Y':
        return 'y'
    elif letter == 'Z':
        return 'z'
    else:
        return "Not Valid"




def isalphabet(letter):
    alphabetletter = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
     'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
     'U', 'V', 'W', 'X', 'Y', 'Z']
    if letter in alphabetletter:
        return True
    else:
        return False

def isdigit(element):
    elements = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
    if element in elements:
        return True
    else:
        return False


def isSpeical(charc):
    alphabetletter = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's',
                      't', 'u', 'v', 'w', 'x', 'y', 'z',
                      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                      'Q', 'R', 'S', 'T','U', 'V', 'W', 'X', 'Y', 'Z']
    numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]

    if charc in alphabetletter or charc in numbers:
        print("The Character is not special")
    else:
        print("The Character is special")


